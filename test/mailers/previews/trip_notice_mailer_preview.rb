# Preview all emails at http://localhost:3000/rails/mailers/trip_notice_mailer
class TripNoticeMailerPreview < ActionMailer::Preview
  def trip_notice
    TripNoticeMailer.trip_notice(User.first, TripNotice.first)
  end
end
