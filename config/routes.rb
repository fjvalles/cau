Rails.application.routes.draw do

  devise_for :users, path_prefix: 'my'

  devise_scope :user do
    authenticated :user do
      root 'users#me', as: :authenticated_root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  resources :zones
  resources :routes
  resources :trip_notices, except: :index do
    get 'search', on: :collection
    get 'send_mail', on: :member
  end
  resources :users do
    collection do
      get 'me'
      resource :medical_record, path: 'me/medical_record'
    end
  end

end
