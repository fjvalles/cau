class Risk < ApplicationRecord
  belongs_to :trip_notice

  validates :name, :prevention, :mitigation, presence: true
end
