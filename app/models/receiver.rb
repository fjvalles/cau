class Receiver < ApplicationRecord

  validates :mail, presence: true, unless: :all_blank?

  def all_blank?
    mail.blank?
  end
end
