class Conveyance < ApplicationRecord
  has_many :trip_conveyances
  has_many :users, through: :trip_conveyances

  enum conveyance_type: [:car, :transit]

  validates :brand, :color, :driver, presence: true, if: :is_car?

  def is_car?
    puts 'CONVEYANCEEEEEEEEEEEEEEE'
    puts conveyance_type
    conveyance_type == 'car' && !all_blank?
  end

  def all_blank?
    color.blank? && brand.blank? && driver.blank?
  end

end
