class TripNotice < ApplicationRecord
  belongs_to :cau_contact, class_name: 'User'
  has_and_belongs_to_many :participants, class_name: 'User'
  has_many :risks, dependent: :destroy
  has_many :integrants, dependent: :destroy
  has_many :receivers, dependent: :destroy
  has_many :itinerary_items, dependent: :destroy
  has_many :trip_conveyances, dependent: :destroy
  has_many :conveyances, through: :trip_conveyances
  has_many :contacts, as: :contactable

  enum activity_type: [:mountaineering, :trekking, :climbing]
  enum status: [:saved, :sent]

  has_attached_file :weather_forecast_img, styles: { medium: '300x300>' }
  validates_attachment :weather_forecast_img, presence: true,
                       content_type: { content_type: /\Aimage\/.*\z/ }

  accepts_nested_attributes_for :itinerary_items, :risks, :integrants, :receivers,
                                :trip_conveyances, :contacts, allow_destroy: true

  scope :search, -> (query) {
    where('route ilike :query or zone ilike :query',
    query: "%#{query}%")
  }

  validates :activity_type, :zone, :cau_contact_id, :participants,
            :itinerary_items, :return_date, :weather_forecast_img,
            presence: true
  validate :return_date_after_itinerary?

  def return_date_after_itinerary?
    itinerary_date = get_latest_date
    if itinerary_date.present?
      if return_date <= itinerary_date
        errors.add :return_date, "Fecha de regreso límite debe ser posterior a fecha de itinerario"
      end
    end
  end

  def get_latest_date
    date = ''
    itinerary_items.each do |i|
      if i.end_date.present?
        date = i.end_date if !date.present?
        date = i.end_date if i.end_date > date
      end
    end
    date
  end

  def activity
    case activity_type
    when 'mountaineering'
      'Montañismo'
    when 'trekking'
      'Senderismo'
    when 'climbing'
      'Escalada'
    end
  end

  def has_conveyances?
    result = false
    if conveyances.present?
      conveyances.each do |c|
        result = true if c.conveyance_type.present?
      end
    end
    result
  end

  def e_contacts?
    result = false
    if contacts.present?
      contacts.each do |c|
        result = true if !c.name.blank? || !c.phone.blank?
      end
    end
    result
  end

  def medical_data?
    result = false
    if integrants.present?
      integrants.each do |i|
        result ||= i.illnesses.present? || i.medication.present? || i.risks.present?
      end
    end
    result ||= participants.map(&:medical_record).any?
  end
end
