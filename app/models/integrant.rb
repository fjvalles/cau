class Integrant < ApplicationRecord
  belongs_to :trip_notice

  validates :name, :phone, :emergency_contact, :contact_phone, presence: true,
            unless: :all_blank?


  def all_blank?
    name.blank? && phone.blank? && emergency_contact.blank? && contact_phone.blank?
  end

end
