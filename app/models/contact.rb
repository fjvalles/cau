class Contact < ApplicationRecord
  belongs_to :contactable, polymorphic: true

  validates :name, :phone, presence: true, unless: :all_blank?

  def all_blank?
    name.blank? && phone.blank?
  end
end
