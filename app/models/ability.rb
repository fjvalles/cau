class Ability
  include CanCan::Ability

  def initialize(user)
    if user.present?
      if user.admin?
        can :manage, [Conveyance, ItineraryItem, Risk, User, TripNotice]
        can [:create, :update, :destroy], MedicalRecord, user_id: user.id
        can :read, MedicalRecord
      elsif user.member?
        can :manage, [Conveyance, ItineraryItem, Risk]
        can [:me, :update, :show], User, id: user.id
        can [:read, :create, :update, :destroy], MedicalRecord, user_id: user.id
        can :create, TripNotice
        can [:send_mail, :read, :update, :search], TripNotice, id: user.trip_notices.pluck(:id)
        can :read, User, active: :true
      else # user is external
        can :create, TripNotice
        can [:send_mail, :read, :update, :search], TripNotice, id: user.trip_notices.pluck(:id)
        can :me, User, id: user.id
      end
    else
      # cannot :manage, :all
    end
  end
end
