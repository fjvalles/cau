class TripConveyance < ApplicationRecord
  belongs_to :trip_notice
  belongs_to :conveyance
  belongs_to :user

  accepts_nested_attributes_for :conveyance, allow_destroy: true, reject_if: proc { |a| a['brand'].blank? || a['color'].blank? || a['driver'].blank? }

end
