class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :medical_record
  has_and_belongs_to_many :trip_notices
  has_many :trip_conveyances
  has_many :supervised_trip_notices, class_name: 'TripNotice'
  has_one :emergency_contact, class_name: 'Contact', as: :contactable, dependent: :destroy

  enum role: [:member, :external, :admin]
  enum gender: [:male, :female]

  has_attached_file :avatar, styles: { medium: '150x150' }
  validates_attachment :avatar, content_type: { content_type: /\Aimage\/.*\z/ }

  validates :first_name, :last_name, presence: true
  # validates :member_number, uniqueness: true, if: :member?

  accepts_nested_attributes_for :emergency_contact, allow_destroy: true

  before_validation :generate_password, on: :create

  def member?
    self.role == 'member'
  end

  def generate_password
    if password.blank?
      self.password = Devise.friendly_token.first(8)
      UserMailer.welcome_email(self, password).deliver_now
    end
  end

  def active_for_authentication?
    super && self.active
  end

  def inactive_message
    "Lo sentimos, su usuario ha sido desactivado."
  end

  def image
    avatar.present? ? avatar(:medium) : 'default-profile.jpg'
  end

  def age
    return 'Desconocida' if birthday.nil?
    today ||= Date.current
    diff = (12 * today.year + today.month) - (12 * birthday.year + birthday.month)
    y, m = diff.divmod 12
    y == 1 ? '1 año' : "#{y} años"
  end

  def seniority
    return '' if date_of_joining.nil?
    year = ' hace '
    months = ' hace '
    years = Date.current.year - date_of_joining.year
    if years == 0
      months = Date.current.month - date_of_joining.month
    else
      years
    end
  end

  def full_name
    last_name.titleize + ', ' + first_name.titleize
  end

  def full_name2
    first_name.titleize + ' ' + last_name.titleize
  end

end
