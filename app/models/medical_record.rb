class MedicalRecord < ApplicationRecord
  belongs_to :user
  has_many :medical_conditions, dependent: :destroy

  enum blood_type: [:o_positive, :o_negative, :a_positive, :a_negative,
                    :b_positive, :b_negative, :ab_positive, :ab_negative]

  accepts_nested_attributes_for :medical_conditions, allow_destroy: true

end
