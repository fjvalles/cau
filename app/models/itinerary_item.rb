class ItineraryItem < ApplicationRecord
  belongs_to :trip_notice

  validates :start_date, :end_date, :activity,
            presence: true
  validate :end_date_after_start_date?

  def end_date_after_start_date?
    if start_date.present? && end_date.present?
      if end_date < start_date
        errors.add :end_date, "Fecha de fin de actividad debe ser posterior a su fecha de inicio"
      end
    end
  end
end
