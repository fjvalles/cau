class ApplicationMailer < ActionMailer::Base
  layout 'mailer'

  def arrival_reminder(user, trip_notice)
    @user = user
    @trip_notice = trip_notice
    mail(to: @user.email,
         subject: "Recordatorio llegada salida a #{@trip_notice.zone}")
  end
end
