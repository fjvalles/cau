class TripNoticeMailer < ApplicationMailer
  def trip_notice(user, trip_notice)
    @user = user
    @trip_notice = trip_notice
    fn = "aviso_de_salida_#{@trip_notice.id}.pdf"
    attachments[fn] = File.read("#{Rails.root}/app/assets/pdfs/#{fn}")
    email_with_name = %("#{@user.full_name2}" <#{@user.email}>)
    mails = 'montanismo-uc@googlegroups.com'
    # mails = 'fjvallesj@gmail.com'
    puts '???? RECEIVERS'
    puts ap @trip_notice.receivers
    @trip_notice.receivers.each do |r|
      mails += ', ' + r.mail
    end
    mail(to: mails,
         from: email_with_name,
         subject: 'Aviso de salida - ' + @trip_notice.zone)
  end

end
