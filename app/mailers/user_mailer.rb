class UserMailer < ApplicationMailer
  def welcome_email(user, pwd)
    @user = user
    @password = pwd
    email_with_name = %("#{@user.full_name2}" <#{@user.email}>)
    if @user.email != ''
      mail(to: email_with_name, from: 'Club Andino Universitario <no-reply@cau.cl>',
        subject: 'Bienvenido al Club Andino Universitario')
    end
  end
end
