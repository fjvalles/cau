$(document).on("turbolinks:load", function() {

  // Tootltip for tabs
  $('a[title]').tooltip();

  // Live search
  $(".search-query").bind("keyup", function() {
    $.ajax({
      url: '/trip_notices/search.js',
      data: { query: $(this).val() },
      type: 'GET'
    });
  });

  var canSendMail = true;
  $("#send-btn").click(function() {
    if (canSendMail) {
      canSendMail = false;
      var tnId = $(this).data('tn-id');
      $.ajax({
        url: '/trip_notices/' + tnId + '/send_mail',
        type: 'GET'
      }).success(function () {
        canSendMail = true;
        $('.tn-item-' + tnId + ' #send-btn-container').html('Enviado');
        window.location = document.location.origin + "/#trip_notices";
        $('#notice').html("<div class='alert fade in alert-success'>\
                             <button type='button' class='close' data-dismiss='alert'>x</button>\
                             Aviso de salida fue enviado exitosamente.\
                           </div>");
        $('.tn-item-' + tnId + ' .edit').hide();
        $('.tn-item-' + tnId + ' .download').show();
        $(".alert" ).fadeOut(3000);
      });
    }
  });

  // Persistent tabs
  if (location.hash != '') {
    $('a[href="'+location.hash+'"]').tab('show');
  }

  $('a[data-toggle="tab"]').on('click', function(e) {
    location.hash = $(this).attr('href').substr(1);
  });

});
