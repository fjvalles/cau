//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require turbolinks
//= require moment
//= require moment/es
//= require bootstrap-datetimepicker
//= require pickers
//= require select2
//= require select2_locale_es
//= require_tree .

$(document).on("turbolinks:load", function() {

  // $('#datetimepicker1').datetimepicker({ inline: true, sideBySide: true });
  // $('.datetimepicker2').datetimepicker({format: 'DD-MM-YYYY HH:ss'});

  $('.datetimepicker').datetimepicker();

  // Fade out for flash messages
  $(".alert" ).fadeOut(3000);

  $(window).scroll(function() {
    if ($(document).scrollTop() > 50) {
      $('nav').addClass('shrink');
    }
    else {
      $('nav').removeClass('shrink');
    }
  });

  $(".user-img .image-preview-input input:file").change(function (){
      var file = this.files[0];
      var reader = new FileReader();
      reader.onload = function (e) {
          $(".image-preview-input-title").text("Cambiar");
          $(".image-preview-clear").show();
          $(".image-preview-filename").val(file.name);
          if (defaultProfile == undefined) {
            defaultProfile = $('#avatar').attr('src');
          }
          $('#avatar').attr('src', e.target.result);
      }
      reader.readAsDataURL(file);
  });

});
var defaultProfile;
