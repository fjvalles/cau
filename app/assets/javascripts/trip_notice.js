$(document).on("turbolinks:load", function() {

  $( ".multi-select" ).select2({ theme: "bootstrap" });

  $('div.product-chooser-item').not('.disabled').on('click', function(){
  	$(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
  	$(this).addClass('selected');
  	$(this).find('input[type="radio"]').prop("checked", true);
  });

  // Create the close button
  var closebtn = $('<button/>', {
      type:"button",
      text: 'x',
      id: 'close-preview',
      style: 'font-size: initial;',
  });
  closebtn.attr("class","close pull-right");
  // Set the popover default content
  $('.image-preview').popover({
      trigger:'manual',
      html:true,
      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
      content: "No hay imagen",
      placement:'bottom'
  });
  // Clear event
  $('.image-preview-clear').click(function(){
      $('.image-preview').attr("data-content","").popover('hide');
      $('.image-preview-filename').val("");
      $('.image-preview-clear').hide();
      $('.image-preview-input input:file').val("");
      $(".image-preview-input-title").text("Buscar");
      $('#avatar').attr('src', defaultProfile);
  });
  // Create the preview image
  $(".weather-img .image-preview-input input:file").change(function (){
      var img = $('<img/>', {
          id: 'dynamic',
          width:250,
          height:200
      });
      var file = this.files[0];
      var reader = new FileReader();
      // Set preview image into the popover data-content
      reader.onload = function (e) {
          $(".image-preview-input-title").text("Cambiar");
          $(".image-preview-clear").show();
          $(".image-preview-filename").val(file.name);
          img.attr('src', e.target.result);
          $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
      }
      reader.readAsDataURL(file);
  });

  $('#zone').change(function() {
    zone = $(this);
    $("#trip_notice_route_id").html('<option value>Ruta</option>');
    if (zone.val() != '') {
      $.ajax({
        url: '/zones/' + zone.val(),
        dataType: 'json',
        delay: 200,
        cache: true,
        success: function(res){
          $.each(res, function(index, value){
            $("#trip_notice_route_id").append('<option value=' + value.id  + '>'+ value.name  +'</option>');
          });
          if($("#trip_notice_route_id").selectpicker){
            $("#trip_notice_route_id").selectpicker('refresh');
          }
        }
      });
    }
  });

  var countFormGroup = function ($form) {
    return $form.find('.form-group').length;
  };

  if($('.multiple-form-group').length > 0) {
    var itinerary = $('.multiple-form-group.itinerary .btn-add'),
        risks = $('.multiple-form-group.risks .btn-add'),
        conveyance = $('.multiple-form-group.conveyance .btn-add'),
        ecs = $('.multiple-form-group.ecs .btn-add'),
        medical = $('.multiple-form-group.medical .btn-add');
        integrants = $('.multiple-form-group.integrants .btn-add');
    if(itinerary.length > 0){updateAddButtons(itinerary);}
    if(risks.length > 0){updateAddButtons(risks);}
    if(conveyance.length > 0){updateAddButtons(conveyance);}
    if(ecs.length > 0){updateAddButtons(ecs);}
    if(medical.length > 0){updateAddButtons(medical);}
    if(integrants.length > 0){updateAddButtons(integrants);}


    var itineraryPanel = itinerary.closest('.panel-default'),
        risksPanel = risks.closest('.panel-default'),
        conveyancePanel = conveyance.closest('.panel-default'),
        ecsPanel = ecs.closest('.panel-default'),
        medicalPanel = medical.closest('.panel-default'),
        integrantsPanel = integrants.closest('.panel-default');

    updateButtons(itineraryPanel);
    updateButtons(risksPanel);
    updateButtons(conveyancePanel);
    updateButtons(ecsPanel);
    updateButtons(medicalPanel);
    updateButtons(integrantsPanel);

  }

  $(document).on('click', '.btn-add', function (e) {
    e.preventDefault();
    //Tomo la instancia del formulario
    var $formGroup = $(this).closest('.form-group');
    var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
    var $formGroupClone = $formGroup.clone();


    if($formGroup.hasClass('itinerary')){
      setNewNestedItem($formGroupClone, "input.activity");
      setNewNestedItem($formGroupClone, "input.start-altitude");
      setNewNestedItem($formGroupClone, "input.end-altitude");
      setNewNestedItem($formGroupClone, "input.start-date");
      setNewNestedItem($formGroupClone, "input.end-date");
    }
    else if($formGroup.hasClass('risks')) {
      setNewNestedItem($formGroupClone, "input.name");
      setNewNestedItem($formGroupClone, "input.altitude");
      setNewNestedItem($formGroupClone, "textarea.prevention");
      setNewNestedItem($formGroupClone, "textarea.mitigation");
      setNewNestedItem($formGroupClone, "textarea.comment");
    }
    else if($formGroup.hasClass('conveyance')) {
      setNewNestedItem($formGroupClone, "select.ctype");
      setNewNestedItem($formGroupClone, "input.brand");
      setNewNestedItem($formGroupClone, "input.model");
      setNewNestedItem($formGroupClone, "input.color");
      setNewNestedItem($formGroupClone, "input.plate");
      setNewNestedItem($formGroupClone, "input.driver");
    }
    else if($formGroup.hasClass('ecs')) {
      setNewNestedItem($formGroupClone, "input.name");
      setNewNestedItem($formGroupClone, "input.email");
      setNewNestedItem($formGroupClone, "input.phone");
      setNewNestedItem($formGroupClone, "input.additional-phone");
    }
    else if($formGroup.hasClass('medical')) {
      setNewNestedItem($formGroupClone, "input.name");
      setNewNestedItem($formGroupClone, "input.medicine");
      setNewNestedItem($formGroupClone, "input.dose");
      setNewNestedItem($formGroupClone, "input.frequency");
      setNewNestedItem($formGroupClone, "textarea.risk");
      setNewNestedItem($formGroupClone, "textarea.side-effects");
    }
    else if($formGroup.hasClass('integrants')) {
      setNewNestedItem($formGroupClone, "input.name");
      setNewNestedItem($formGroupClone, "input.phone");
      setNewNestedItem($formGroupClone, "input.emergency_contact");
      setNewNestedItem($formGroupClone, "input.contact_phone");
      setNewNestedItem($formGroupClone, "input.illnesses");
      setNewNestedItem($formGroupClone, "input.medication");
      setNewNestedItem($formGroupClone, "input.risks");
    }

    $formGroupClone.find('input').val('');
    $formGroupClone.find('textarea').val('');
    $formGroupClone.insertAfter($formGroup);
    if($formGroup.hasClass('itinerary')) {
      $('.datetimepicker').datetimepicker();
    }

    var $panel = $formGroup.closest('.panel-default');
    updateButtons($panel);

    $(this).off('click');
  });


  function updateButtons(panel) {
    var btnRemove;
    if (panel.children().not(':hidden').length == 1) {
      btnRemove = panel.children().not(':hidden').closest('.multiple-form-group').children(':last')[0].getElementsByClassName('btn-remove');
      $(btnRemove).prop('disabled', true);
    } else {
      for (var i = 0; i < panel.children().not(':hidden').length; i++){
        var form = panel.children().not(':hidden')[i].closest('.multiple-form-group');
        btnRemove = form.getElementsByClassName('btn-remove');
        $(btnRemove).prop('disabled', false);
      }
    }
  }


  $(document).on('click', '.btn-remove', function (e) {
    e.preventDefault();

    var $formGroup = $(this).closest('.form-group');
    var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

    var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
    if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
      $lastFormGroupLast.find('.btn-add').attr('disabled', false);
    }
    var name = $formGroup.find('input').first().attr('name');
    if (name) {
      var split = name.split('[');
      name = name.replace(split[split.length -1], '_destroy]');
      $("<input type='hidden' name='" + name + "' value='1'>").insertAfter($formGroup);
    }
    var $panel = $formGroup.closest('.panel-default');

    $formGroup.remove();

    updateButtons($panel);

  });

  $('')

  if($('.ctype').val() == 'car') {
    $('.brand, .color, .model, .plate, .driver').show();
    $('.company, .number').hide();
  }
  else {
    $('.brand, .color, .model, .driver, .plate').hide();
    $('.company, .number').show();
  }

  $(document).on('change', '.ctype', function () {
    if($(this).val() == 'car') {
      $(this).closest('.form-group').find('.brand, .color, .model, .plate, .driver').show();
      $(this).closest('.form-group').find('.company, .number').hide();
    }
    else {
      $(this).closest('.form-group').find('.brand, .color, .model, .plate, .driver').hide();
      $(this).closest('.form-group').find('.company, .number').show();
    }
  });

});

function setNewNestedItem(group, item) {
  var name = group.find(item).attr('name');
  var id = group.find(item).attr('id');
  var index = parseInt(name.split("[")[2].replace(']',''));

  name = name.replace(index.toString(), (index+1).toString());
  group.find(item).attr('name', name);
  id = id.replace(index.toString(), (index+1).toString());
  group.find(item).attr('id', id);

  // if (item === ('input.start-date' || 'input.end-date')){
  //   var itemCopy = $(input.)
  //   // var name = group.find(item).attr('name');
  //   // var id = group.find(item).attr('id');
  //   // var index = parseInt(name.split("[")[2].replace(']',''));
  //   //
  //   // name = name.replace(index.toString(), (index+1).toString());
  //   // group.find(item).attr('name', name);
  //   // id = id.replace(index.toString(), (index+1).toString());
  //   // group.find(item).attr('id', id);
  // }

}

function updateAddButtons(group) {
  group.not(':last').prop('disabled', true);
}

$(document).on('click', '#close-preview', function(){
  $('.image-preview').popover('hide');
  // Hover befor close the preview
  $('.image-preview').hover(
      function () {
         $('.image-preview').popover('show');
      },
       function () {
         $('.image-preview').popover('hide');
      }
  );
});
