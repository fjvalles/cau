class TripNoticePdf < Prawn::Document
  def initialize(trip_notice)
    super()
    @trip_notice = trip_notice
    content
    emergency
  end

  def content
    font_size 12
    image "#{Rails.root}/app/assets/images/logoCAU.png", width: 120, position: :center
    move_down 5
    text "<b>Aviso de Actividad de Montaña</b>", inline_format: true, align: :center
    move_down 10

    font_size 9
    text "Contacto CAU: " + @trip_notice.cau_contact.full_name2, align: :right
    text "Teléfono contacto CAU: " + @trip_notice.cau_contact.phone, align: :right
    text "Email contacto CAU: " + @trip_notice.cau_contact.email, align: :right
    text "Fecha de regreso: " + @trip_notice.return_date.strftime('%e %b %Y'), align: :right
    text "Hora de regreso: " + @trip_notice.return_date.strftime('%H:%M'), align: :right
    stroke_horizontal_rule
    move_down 10

    text "Actividad: " + @trip_notice.activity
    text "Nombre cerro o sector: " + @trip_notice.zone
    text "Ruta: " + @trip_notice.route if @trip_notice.route.present?
    if @trip_notice.weather_forecast_link.present?
      text "Enlace pronóstico del tiempo: <u><link target='_blank' href='" +
           @trip_notice.weather_forecast_link + "' >Pronóstico</link></u>", inline_format: true
    end
    move_down 10

    text "<b>Participantes</b>", inline_format: true, size: 11
    data = [['Nombre', 'Teléfono', 'Contacto emergencia (C.E.)', 'Teléfono C.E.']]
    @trip_notice.participants.each_with_index do |p, i|
      if p.email != 'exalumno@cau.cl'
        if p.emergency_contact
          data += [[p.full_name2, p.phone, p.emergency_contact.name, p.emergency_contact.phone]]
        else
          data += [[p.full_name2, p.phone, "", ""]]
        end
      end
    end
    @count = @trip_notice.participants.size
    @trip_notice.integrants.each_with_index do |p, i|
      data += [[p.name, p.phone, p.emergency_contact, p.contact_phone]]
    end
    table(data, header: true, width:515, :cell_style => { :border_widths => 0.01 })
    move_down 10


    text "<b>Itinerario</b>", inline_format: true, size: 11
    data = [['Actividad', { content: 'Jornada (inicio/fin)', colspan: 2 },
           { content: 'Altitud (inicial/final)', colspan: 2 }]]
    @trip_notice.itinerary_items.each do |i|
      data += [[{ content: i.activity, rowspan: 2 }, 'inicio', i.start_date.strftime('%e %b %Y %H:%M'), 'inicio', i.start_altitude]] +
              [['fin', i.end_date.strftime('%e %b %Y %H:%M'), 'fin', i.end_altitude]]
    end
    table(data, header: true, width:515, :cell_style => { :border_widths => 0.01 })
    move_down 10

    text "<b>Riesgos</b>", inline_format: true, size: 11
    data = [['Nombre', 'Zona/Waypoint', 'Prevención', 'Mitigación', 'Comentario']]
    @trip_notice.risks.each do |r|
      data += [[r.name, r.altitude, r.prevention, r.mitigation, r.comment]]
    end
    table(data, header: true, width:515, :cell_style => { :border_widths => 0.01 })
    move_down 10

    text "<b>Pronóstico del tiempo</b>", inline_format: true, size: 11
    image @trip_notice.weather_forecast_img.path, position: :center, height: 250
    move_down 10

    if @trip_notice.alternative_itinerary_comment.present?
      text "<b>Itinerario alternativo</b>", inline_format: true, size: 11
      text @trip_notice.alternative_itinerary_comment
      move_down 10
    end

    if @trip_notice.has_conveyances?
      text "<b>Transporte</b>", inline_format: true, size: 11
      data = [['Marca', 'Modelo', 'Color', 'Patente', 'Conductor', 'Empresa', 'Recorrido']]
      @trip_notice.conveyances.each do |c|
        if c.car?
          data += [[c.brand, c.model, c.color, c.plate, c.driver]]
        else
          data += [['', '', '', '', '', c.company, c.number]]
        end
      end
      table(data, header: true, width: 515, cell_style: { border_widths: 0.01 })
      move_down 10
    end

    if @trip_notice.observations.present?
      text "<b>Observaciones</b>", inline_format: true, size: 11
      text @trip_notice.observations
      move_down 10
    end

    if @trip_notice.medical_data?
      text "<b>Datos médicos</b>", inline_format: true, size: 11
      data = [['Nombre', 'Enfermedad/Alergia/Operación', 'Medicamentos habituales', 'Riesgos/ comentarios']]
      @trip_notice.participants.map(&:medical_record).each do |mr|
        if mr
          mr.medical_conditions.each do |mc|
            typical_medicine = mc.medicine + ' ' + mc.dose
            if mc.comment
              risks = mc.risk + '/ ' + mc.comment
            else
              risks = mc.risk
            end
            data += [[mr.user.full_name2, mc.name, typical_medicine, risks]]
          end
        end
      end
      @trip_notice.integrants.each do |p|
        if (!p.illnesses.empty? || !p.medication.empty? || !p.risks.empty?)
          data += [[p.name, p.illnesses, p.medication, p.risks]]
        end
      end
      table(data, header: true, width:515, cell_style: { border_widths: 0.01 }) if data.length > 1
      move_down 10
    end

    if @trip_notice.e_contacts?
      text "<b>Números de contacto/emergencia</b>", inline_format: true, size: 11
      data = [['Nombre', 'Correo electrónico', 'Teléfono', 'Teléfono adicional']]
      @trip_notice.contacts.each do |c|
        data += [[c.name, c.email, c.phone, c.additional_phone]]
      end
      table(data, header: true, width: 515, cell_style: { border_widths: 0.01 })
    end

  end

  def emergency
    move_down 30

    text "<b>Información relevante</b>", inline_format: true, size: 12, align: :center
    move_down 8

    text "<b>Cuerpos de rescate oficiales</b>", inline_format: true, size: 11
    data = [['Nombre', 'Teléfono', 'Teléfono adicional']] +
           [['Carabineros', '+56229220000', '']] +
           [['Socorro Andino', '+56226994764', '+56226989094']]
    table(data, header: true, width:515, :cell_style => { :border_widths => 0.01 })
    move_down 10

    text "<b>Responsabilidad de la cordada:</b>", inline_format: true, size: 11
    move_down 6
    text "- Hacer su aviso de salida de forma completa y responsable y enviarlo al egroup Montañismo UC."
    text "- Buscar alguien responsable y que se encuentre disponible y dispuesto a ejercer la función de Contacto CAU para su salida."
    text "- Informar a su contacto CAU sobre los detalles de la salida, su motivación y sus ambiciones."
    text "- Tomar decisiones en terreno que respeten la hora de retorno señalada para evitar la activación de los protocolos de emergencia y el gasto de recursos económicos y humanos de forma innecesaria."
    text "- Dar aviso de su retorno antes de la hora señalada en el aviso de salida"
    text "- En caso de accidente dar primeros auxilios al accidentado y luego comunicarse directamente con el contacto CAU quién activará el protocolo de emergencia."
    move_down 6

    text "<b>Responsabilidad del contacto CAU:</b>", inline_format: true, size: 11
    move_down 6
    text "- Informarse de los detalles de la salida de la cordada a monitorear."
    text "- Tener conocimiento del protocolo de emergencia a activar en caso de emergencia."
    text "- Estar disponible y “contactable” para verificar el retorno o no retorno de la cordada"
    text "- Dar aviso del retorno o no retorno de la cordada según corresponda"
    text "- En caso de no retorno o accidente confirmado activar el protocolo de emergencia"
    move_down 6

    text "<b>Protocolo de emergencia para contacto CAU<b>", inline_format: true, size: 11
    move_down 6
    text "<b>* Protocolo de emergencia completo disponible en cau.cl</b>", inline_format: true
    move_down 4
    indent(15) do
      text "1) Caso de No Retorno:"
    end
    indent(22) do
      text "- Intentar comunicación o seguimiento de la cordada a través del dispositivo InReach y de la página MapShare."
      text "- En caso de no tener información, dar aviso a los cuerpos oficiales de rescate (CSA y GOPE) del no retorno de la cordada entregando toda la información recopilada en el aviso de salida."
      text "- Preguntar a los cuerpos de rescate qué tipo de información, recursos humanos, técnicos o de equipo podría aportar el CAU en el procedimiento."
      text "- Dar aviso por email al e-group Montañismo UC de la situación y solicitar apoyo de lo señalado por los cuerpos de rescate. Convocar un grupo de búsqueda que pueda prepararse y permanecer “en espera” en caso de ser requerido."
      text "- Avisar a los contactos de emergencia de la situación, explicándoles que lo más posible es que se trate de un retraso y no de un accidente. Informar que ya se le dio aviso a los cuerpos de rescate y que el CAU esta organizándose un grupo de apoyo para lo que sea requerido por los cuerpos de rescate."
      text "- Mantener al club informado a través del e-group de los acontecimientos importantes"
    end
    move_down 6
    indent(15) do
      text "2) Caso de Accidente Confirmado"
    end
    indent(22) do
      text "- Mantener la calma y obtener información de cómo sucedió el accidente, número de accidentados, lesiones diagnosticadas, ubicación geográfica, la atención de primeros auxilios entregada y de la gravedad de la situación."
      text "- Pedir al que da aviso de accidente que en lo posible permanezca disponible como primera fuente en caso de requerir más información por los cuerpos de rescate."
      text "- Dar aviso a los cuerpos oficiales de rescate (CSA y GOPE) del accidente de la cordada entregando toda la información entregada por la cordada y la recopilada en el aviso de salida. "
      text "- Preguntar a los cuerpos de rescate qué tipo de información, recursos humanos, técnicos o de equipo podría aportar el CAU en el procedimiento."
      text "- Dar aviso por email al e-group Montañismo UC de la situación y solicitar apoyo de lo señalado por los cuerpos de rescate. Convocar un grupo de rescate que pueda prepararse y permanecer “en espera” en caso de ser requerido."
      text "- Avisar a los contactos de emergencia de la situación. Informar que ya se le dio aviso a los cuerpos de rescate y que el CAU esta organizándose un grupo de apoyo para lo que sea requerido por los cuerpos de rescate. Mantengan la calma"
      text "- Mantener al club informado a través del e-group de los acontecimientos importantes"
    end

  end
end
