class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.all.order(last_name: :asc)
  end

  def show
    if @user.id == current_user.id
      redirect_to me_users_url
    else
      render action: :me
    end
  end

  def me
    @user = current_user
    @user = User.find(params[:id]) if params[:id].present?
    @trip_notices = @user.trip_notices.order(updated_at: :desc)
  end

  def new
    @user = User.new
    @user.build_emergency_contact unless @user.emergency_contact.present?
  end

  def edit
    @user.build_emergency_contact unless @user.emergency_contact.present?
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to me_users_url, notice: 'Usuario fue creado exitosamente.'
    else
      render :new
    end
  end

  def update
    if @user.update(user_params)
      redirect_to me_users_url, notice: 'Usuario fue actualizado exitosamente.'
    else
      puts ap @user.errors.messages
      render :edit
    end
  end

  def destroy
    @user.destroy
    redirect_to users_url, notice: 'Usuario fue eliminado exitosamente.'
  end

  private

  def user_params
    if current_user.member?
      params.require(:user)
            .permit(:id, :first_name, :last_name, :phone, :birthday, :gender,
                    :avatar, emergency_contact_attributes: [:id, :name, :email,
                    :phone, :additional_phone, :_destroy])
    elsif current_user.admin?
      params.require(:user)
            .permit(:id, :first_name, :last_name, :phone, :birthday, :gender,
                    :avatar, :active, :role, :member_number, :email,
                    :date_of_joining, :password, :password_confirmation,
                    emergency_contact_attributes: [:id, :name, :email, :phone,
                    :additional_phone, :_destroy])
    end
  end

  def set_user
    @user = User.find(params[:id])
  end
end
