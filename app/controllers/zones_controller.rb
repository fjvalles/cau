class ZonesController < ApplicationController
  before_action :set_zone, only: [:show, :edit, :update, :destroy]

  def index
    @zones = Zone.all
  end

  def show
    render json: @zone.routes.to_json
  end

  def me
    @zone = current_zone
  end

  def new
    @zone = Zone.new
  end

  def edit
  end

  def create
    @zone = Zone.new(zone_params)
    if @zone.save
      redirect_to @zone, notice: 'Zona fue creada exitosamente.'
    else
      render :new
    end
  end

  def update
    if @zone.update(zone_params)
      redirect_to @zone, notice: 'Zona fue actualizada exitosamente.'
    else
      render :edit
    end
  end

  def destroy
    @zone.destroy
    redirect_to zones_url, notice: 'Zona fue eliminada exitosamente.'
  end

  private

  def zone_params
    params.require(:zone).permit(:first_name, :last_name, :phone, :email, :active)
  end

  def set_zone
    @zone = Zone.find(params[:id])
  end
end
