class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  load_and_authorize_resource unless: :invalid_controller?
  before_action :authenticate_user!
  layout :layout_by_resource

  rescue_from CanCan::AccessDenied do
    puts '########'
    puts "######## No puede realizar la acción: <#{params[:action]}> del controlador: <#{params[:controller]}>"
    puts '########'
    redirect_to authenticated_root_url, alert: 'No puede realizar la acción requerida.'
  end

  protected

  def invalid_controller?
    devise_controller?
  end


  def layout_by_resource
    if user_signed_in?
      'application'
    else
      'log_in'
    end
  end
end
