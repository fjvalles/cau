class MedicalRecordsController < ApplicationController
  before_action :set_medical_record, only: [:show, :edit, :update, :destroy]

  def show
  end

  def new
    @medical_record = MedicalRecord.new
    @medical_record.medical_conditions.build
  end

  def edit
  end

  def create
    @medical_record = MedicalRecord.new(medical_record_params)
    @medical_record.user = current_user
    if @medical_record.save
      redirect_to me_users_url, anchor: 'medical_record', notice: 'Ficha médica fue creada exitosamente.'
    else
      render :new
    end
  end

  def update
    if @medical_record.update(medical_record_params)
      redirect_to me_users_url, anchor: 'medical_record', notice: 'Ficha médica fue actualizada exitosamente.'
    else
      render :edit
    end
  end

  def destroy
    @medical_record.destroy
    redirect_to me_users_url, anchor: 'medical_record', notice: 'Ficha médica fue eliminada exitosamente.'
  end

  private

  def medical_record_params
    params.require(:medical_record)
          .permit(:blood_type, :medical_insurance, :health_insurance,
                  :prefered_medical_center, medical_conditions_attributes: [:id,
                  :name, :risk, :medicine, :dose, :frequency, :side_effects,
                  :comment, :_destroy])
  end

  def set_medical_record
    @medical_record = current_user.medical_record
  end
end
