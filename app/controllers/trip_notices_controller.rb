class TripNoticesController < ApplicationController
  before_action :set_trip_notice, only: [:show, :edit, :update, :destroy, :send]

  def index
    @trip_notices = current_user.trip_notices
  end

  def show
    pdf = TripNoticePdf.new(@trip_notice).render
    File.open("app/assets/pdfs/aviso_de_salida_#{@trip_notice.id}.pdf", 'wb') do |f|
      f.write(pdf)
    end
    filename = 'aviso_de_salida_' + @trip_notice.id.to_s + '.pdf'
    pdf_filename = File.join(Rails.root, 'app/assets/pdfs/' + filename)
    # filename = 'aviso_de_salida_' + @trip_notice.id.to_s + '.pdf'
    # puts filename
    # pdf_filename = File.join(Rails.root, 'app/assets/pdfs/' + filename)
    # # pdf_filename = TripNoticePdf.new(@trip_notice).render
    disposition = 'inline'
    disposition = 'attachment' if params[:request] == 'download'
    send_file(pdf_filename, filename: filename, disposition: disposition,
      type: 'application/pdf') if params[:request] == 'preview'
    send_file(pdf_filename, filename: filename, type: 'application/pdf') if params[:request] == 'download'
  end

  def new
    @trip_notice ||= TripNotice.new
    @trip_notice.risks.build unless @trip_notice.risks.present?
    @trip_notice.integrants.build unless @trip_notice.integrants.present?
    @trip_notice.receivers.build unless @trip_notice.receivers.present?
    @trip_notice.contacts.build unless @trip_notice.contacts.present?
    @trip_notice.itinerary_items.build unless @trip_notice.itinerary_items.present?
    tc = @trip_notice.trip_conveyances.build unless @trip_notice.trip_conveyances.present?
    tc.build_conveyance if tc.present? && !tc.conveyance.present?
  end

  def edit
    @trip_notice ||= TripNotice.new
    @trip_notice.risks.build unless @trip_notice.risks.present?
    @trip_notice.integrants.build unless @trip_notice.integrants.present?
    @trip_notice.receivers.build unless @trip_notice.receivers.present?
    @trip_notice.contacts.build unless @trip_notice.contacts.present?
    @trip_notice.itinerary_items.build unless @trip_notice.itinerary_items.present?
    tc = @trip_notice.trip_conveyances.build unless @trip_notice.trip_conveyances.present?
    tc.build_conveyance if tc.present? && !tc.conveyance.present?
  end

  def create
    @trip_notice = TripNotice.new(trip_notice_params)
    if @trip_notice.save
      redirect_to me_users_url(anchor: 'trip_notices'), notice: 'Aviso de salida fue creado exitosamente.'
    else
      puts ap @trip_notice.errors.messages
      render :new
    end
  end

  def update
    if current_user.admin? || !@trip_notice.sent?
      if @trip_notice.update(trip_notice_params)
        redirect_to me_users_url(anchor: 'trip_notices'), notice: 'Aviso de salida fue actualizado exitosamente.'
      else
        puts ap @trip_notice.errors.messages
        render :edit
      end
    else
      redirect_to me_users_url(anchor: 'trip_notices'), alert: 'El aviso de salida ya fue enviado y no se puede editar.'
    end
  end

  def search
    @trip_notices = current_user.trip_notices
    @query = params[:query]
    @trip_notices = @trip_notices.search(@query) if @query != ''
  end

  def send_mail
    pdf = TripNoticePdf.new(@trip_notice).render
    File.open("app/assets/pdfs/aviso_de_salida_#{@trip_notice.id}.pdf", 'wb') do |f|
      f.write(pdf)
    end
    TripNoticeMailer.trip_notice(current_user, @trip_notice).deliver_now
    @trip_notice.update(status: :sent)
    filename = 'aviso_de_salida_' + @trip_notice.id.to_s + '.pdf'
    pdf_filename = File.join(Rails.root, 'app/assets/pdfs/' + filename)
    # send_file(pdf_filename, filename: filename, type: 'application/pdf')
    redirect_to trip_notice_url(@trip_notice)
  end

  private

  def set_trip_notice
    @trip_notice = TripNotice.find(params[:id])
  end

  def trip_notice_params
    params.require(:trip_notice)
    .permit(:activity_type, :return_date, :weather_forecast_img,
            :weather_forecast_link, :observations, :route, :zone, :cau_contact_id,
            :alternative_itinerary_comment, participant_ids: [],
            integrants_attributes: [:id, :name, :phone, :emergency_contact, :contact_phone,
              :illnesses, :medication, :risks, :_destroy],
            receivers_attributes: [:id, :mail, :_destroy],
            risks_attributes: [:id, :name, :altitude, :prevention, :mitigation,
            :comment, :_destroy],
            contacts_attributes: [:id, :name, :email, :phone, :additional_phone,
            :_destroy],
            itinerary_items_attributes: [:id, :activity, :start_date, :end_date,
            :start_altitude, :end_altitude, :_destroy],
            trip_conveyances_attributes: [:id, :user_id, conveyance_attributes: [:id,
            :conveyance_type, :brand, :model, :driver, :color, :plate, :company, :number,
            :_destroy]])
  end
end
