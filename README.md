# CAU
---

## Using this project

Make sure you have the following programs installed:

- Rails 5.0.0
- Ruby 2.3.1

Next, to use the Rails app, you need to create:

- config/database.yml:
```ruby
  default: &default
    adapter: postgresql
    encoding: unicode
    host: localhost
    username: username
    password: password
    pool: 5

  development:
    <<: *default
    database: database_name_development
```
- config/secrets.yml:
```ruby
  development:
    secret_key_base: secret_key

  test:
    secret_key_base: secret_key

  production:
    secret_key_base: <%= ENV["SECRET_KEY_BASE"] %>
```
\* You can generate a secret key running `rake secret`

and then run:
```
bundle install
rails db:create db:migrate db:seed
```
You are now ready to run the app locally with `rails s`

---
Developed by **_Francisco Vallés_** y **Daniela Quiroz**
