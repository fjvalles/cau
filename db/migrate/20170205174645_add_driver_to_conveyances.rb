class AddDriverToConveyances < ActiveRecord::Migration[5.0]
  def change
    add_column :conveyances, :driver, :string
  end
end
