class AddTripNoticeToIntegrants < ActiveRecord::Migration[5.0]
  def change
    add_reference :integrants, :trip_notice, foreign_key: true
  end
end
