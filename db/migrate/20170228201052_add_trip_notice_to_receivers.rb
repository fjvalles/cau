class AddTripNoticeToReceivers < ActiveRecord::Migration[5.0]
  def change
    add_reference :receivers, :trip_notice, foreign_key: true
  end
end
