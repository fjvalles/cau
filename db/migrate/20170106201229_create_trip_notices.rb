class CreateTripNotices < ActiveRecord::Migration[5.0]
  def change
    create_table :trip_notices do |t|
      t.integer :status, default: 'saved'
      t.integer :activity_type, default: 0
      t.datetime :return_date
      t.string :weather_forecast_link
      t.attachment :weather_forecast_img
      t.text :observations
      t.text :alternative_itinerary_comment
      t.string :route
      t.string :zone
      t.references :cau_contact, index: true

      t.timestamps
    end
  end
end
