class CreateItineraryItems < ActiveRecord::Migration[5.0]
  def change
    create_table :itinerary_items do |t|
      t.datetime :start_date
      t.datetime :end_date
      t.integer :start_altitude
      t.integer :end_altitude
      t.string :activity
      t.references :trip_notice, foreign_key: true

      t.timestamps
    end
  end
end
