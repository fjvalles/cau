class CreateRisks < ActiveRecord::Migration[5.0]
  def change
    create_table :risks do |t|
      t.string :name
      t.integer :altitude
      t.text :prevention
      t.text :mitigation
      t.text :comment
      t.references :trip_notice, foreign_key: true

      t.timestamps
    end
  end
end
