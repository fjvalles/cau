class CreateTripNoticesUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :trip_notices_users do |t|
      t.references :user, foreign_key: true
      t.references :trip_notice, foreign_key: true
    end
  end
end
