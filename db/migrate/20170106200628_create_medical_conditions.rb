class CreateMedicalConditions < ActiveRecord::Migration[5.0]
  def change
    create_table :medical_conditions do |t|
      t.string :name
      t.text :risk
      t.string :medicine
      t.string :dose
      t.text :side_effects
      t.text :comment
      t.references :medical_record, foreign_key: true

      t.timestamps
    end
  end
end
