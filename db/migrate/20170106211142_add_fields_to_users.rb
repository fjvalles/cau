class AddFieldsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :phone, :string, unique: true
    add_column :users, :birthday, :date
    add_column :users, :gender, :integer
    add_column :users, :role, :integer, default: 0
    add_column :users, :member_number, :integer, unique: true
    add_column :users, :active, :boolean, default: true
    add_attachment :users, :avatar
    add_column :users, :date_of_joining, :date
  end
end
