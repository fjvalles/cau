class CreateConveyances < ActiveRecord::Migration[5.0]
  def change
    create_table :conveyances do |t|
      t.integer :conveyance_type
      t.string :brand
      t.string :model
      t.string :color
      t.string :plate
      t.string :company
      t.string :number

      t.timestamps
    end
  end
end
