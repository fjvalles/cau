class CreateIntegrants < ActiveRecord::Migration[5.0]
  def change
    create_table :integrants do |t|
      t.string :name
      t.string :phone
      t.string :emergency_contact
      t.string :contact_phone
      t.string :illnesses
      t.string :medication
      t.string :risks

      t.timestamps
    end
  end
end
