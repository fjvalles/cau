class CreateMedicalRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :medical_records do |t|
      t.integer :blood_type
      t.string :medical_insurance
      t.string :health_insurance
      t.string :prefered_medical_center
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
