class CreateTripConveyance < ActiveRecord::Migration[5.0]
  def change
    create_table :trip_conveyances do |t|
      t.references :trip_notice, foreign_key: true
      t.references :conveyance, foreign_key: true
      t.references :user, foreign_key: true
    end
  end
end
