# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170228201052) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contacts", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "additional_phone"
    t.string   "email"
    t.string   "contactable_type"
    t.integer  "contactable_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["contactable_type", "contactable_id"], name: "index_contacts_on_contactable_type_and_contactable_id", using: :btree
  end

  create_table "conveyances", force: :cascade do |t|
    t.integer  "conveyance_type"
    t.string   "brand"
    t.string   "model"
    t.string   "color"
    t.string   "plate"
    t.string   "company"
    t.string   "number"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "driver"
  end

  create_table "integrants", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "emergency_contact"
    t.string   "contact_phone"
    t.string   "illnesses"
    t.string   "medication"
    t.string   "risks"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "trip_notice_id"
    t.index ["trip_notice_id"], name: "index_integrants_on_trip_notice_id", using: :btree
  end

  create_table "itinerary_items", force: :cascade do |t|
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer  "start_altitude"
    t.integer  "end_altitude"
    t.string   "activity"
    t.integer  "trip_notice_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["trip_notice_id"], name: "index_itinerary_items_on_trip_notice_id", using: :btree
  end

  create_table "medical_conditions", force: :cascade do |t|
    t.string   "name"
    t.text     "risk"
    t.string   "medicine"
    t.string   "dose"
    t.text     "side_effects"
    t.text     "comment"
    t.integer  "medical_record_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["medical_record_id"], name: "index_medical_conditions_on_medical_record_id", using: :btree
  end

  create_table "medical_records", force: :cascade do |t|
    t.integer  "blood_type"
    t.string   "medical_insurance"
    t.string   "health_insurance"
    t.string   "prefered_medical_center"
    t.integer  "user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["user_id"], name: "index_medical_records_on_user_id", using: :btree
  end

  create_table "receivers", force: :cascade do |t|
    t.string   "mail"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "trip_notice_id"
    t.index ["trip_notice_id"], name: "index_receivers_on_trip_notice_id", using: :btree
  end

  create_table "risks", force: :cascade do |t|
    t.string   "name"
    t.integer  "altitude"
    t.text     "prevention"
    t.text     "mitigation"
    t.text     "comment"
    t.integer  "trip_notice_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["trip_notice_id"], name: "index_risks_on_trip_notice_id", using: :btree
  end

  create_table "trip_conveyances", force: :cascade do |t|
    t.integer "trip_notice_id"
    t.integer "conveyance_id"
    t.integer "user_id"
    t.index ["conveyance_id"], name: "index_trip_conveyances_on_conveyance_id", using: :btree
    t.index ["trip_notice_id"], name: "index_trip_conveyances_on_trip_notice_id", using: :btree
    t.index ["user_id"], name: "index_trip_conveyances_on_user_id", using: :btree
  end

  create_table "trip_notices", force: :cascade do |t|
    t.integer  "status",                            default: 0
    t.integer  "activity_type",                     default: 0
    t.datetime "return_date"
    t.string   "weather_forecast_link"
    t.string   "weather_forecast_img_file_name"
    t.string   "weather_forecast_img_content_type"
    t.integer  "weather_forecast_img_file_size"
    t.datetime "weather_forecast_img_updated_at"
    t.text     "observations"
    t.text     "alternative_itinerary_comment"
    t.string   "route"
    t.string   "zone"
    t.integer  "cau_contact_id"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.index ["cau_contact_id"], name: "index_trip_notices_on_cau_contact_id", using: :btree
  end

  create_table "trip_notices_users", force: :cascade do |t|
    t.integer "user_id"
    t.integer "trip_notice_id"
    t.index ["trip_notice_id"], name: "index_trip_notices_users_on_trip_notice_id", using: :btree
    t.index ["user_id"], name: "index_trip_notices_users_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",   null: false
    t.string   "encrypted_password",     default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.date     "birthday"
    t.integer  "gender"
    t.integer  "role",                   default: 0
    t.integer  "member_number"
    t.boolean  "active",                 default: true
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.date     "date_of_joining"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "integrants", "trip_notices"
  add_foreign_key "itinerary_items", "trip_notices"
  add_foreign_key "medical_conditions", "medical_records"
  add_foreign_key "medical_records", "users"
  add_foreign_key "receivers", "trip_notices"
  add_foreign_key "risks", "trip_notices"
  add_foreign_key "trip_conveyances", "conveyances"
  add_foreign_key "trip_conveyances", "trip_notices"
  add_foreign_key "trip_conveyances", "users"
  add_foreign_key "trip_notices_users", "trip_notices"
  add_foreign_key "trip_notices_users", "users"
end
