puts 'Ejecutando seed'

User.create!(first_name:'Ex-Alumno', last_name:'Talleres', gender: :male,
             email:'exalumno@cau.cl', phone:'+56900000000', password:'123123123',
             role: :external, birthday: Date.current)
User.create!(first_name:'Usuario', last_name:'Administrador', gender: :male,
             email:'admin@cau.cl', phone:'+56911111111', password:'123123123',
             role: :admin, birthday: Date.current)
#
# require 'csv'
# counter = 0
# puts '--------------------'
# puts '# Listado de socios que no se pudieron crear (no tienen correo)'
# puts '--------------------'
# CSV.foreach('socios.csv', headers: true) do |row|
#   data_hash = row.to_hash
#   first_name = data_hash["Nombres"].present? ? data_hash["Nombres"] : ''
#   phone = data_hash["Teléfono"].present? ? data_hash["Teléfono"] : ''
#   email = data_hash["Correo"].present? ? data_hash["Correo"] : ''
#   last_name = data_hash["Apellido paterno"] + ' ' if data_hash["Apellido paterno"].present?
#   last_name += data_hash["Apellido materno"] if data_hash["Apellido materno"].present?
#   u = User.find_or_create_by(first_name: first_name, last_name: last_name,
#                              email: email, phone: phone, role: :member)
#   puts data_hash if u.errors.present?
#   puts u.errors.messages if u.errors.present?
#   puts ' ' if u.errors.present?
#   counter += 1
# end
#
puts 'Fin seed'
